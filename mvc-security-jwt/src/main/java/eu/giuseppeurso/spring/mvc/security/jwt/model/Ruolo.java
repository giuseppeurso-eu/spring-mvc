package eu.giuseppeurso.spring.mvc.security.jwt.model;

public class Ruolo {

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_API = "API";
    public static final String ROLE_UI = "UI";

}
