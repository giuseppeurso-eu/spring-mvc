package eu.giuseppeurso.spring.mvc.security.jwt.utils;

public enum HeaderUtil {
    ACCESS_TOKEN("access_token"),
    REFRESH_TOKEN("refresh_token");

    private final String value;

    HeaderUtil(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
