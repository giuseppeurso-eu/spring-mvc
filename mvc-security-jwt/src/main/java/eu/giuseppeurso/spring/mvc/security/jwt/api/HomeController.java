package eu.giuseppeurso.spring.mvc.security.jwt.api;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import eu.giuseppeurso.spring.mvc.security.jwt.model.Utente;
import eu.giuseppeurso.spring.mvc.security.jwt.service.UtenteService;
import eu.giuseppeurso.spring.mvc.security.jwt.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Enumeration;

@RestController
@RequestMapping("/home")
//@RequiredArgsConstructor
//@Slf4j
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);


    @Autowired
    private UtenteService utenteService;

    // Get specific user
    @GetMapping("/{id}")
    public ResponseEntity<Utente> testController(@PathVariable String id, HttpServletRequest request) throws BadJOSEException, ParseException, JOSEException {
        Enumeration<String> reqAtt = request.getSession().getAttributeNames();


        while (reqAtt.hasMoreElements()) {
            String param = reqAtt.nextElement();
            logger.info( "Req Attr: "+param);
        }

//        request.getSession().getAttributeNames();
//        User user = userService.findByUsername(id);
//        user.setPassword("*******");
//        return ResponseEntity.ok().body(user);

        String reqToken = request.getHeader("Authorization").split(" ")[1];
        logger.info("TOKEN: "+reqToken);
        //logger.info("Strip TOKEN: "+JwtUtil.stripBase64(reqToken));
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        logger.info("TOKEN (json): "+JwtUtil.stripToken(reqToken));


        Utente user = utenteService.findByUsername(id);
        return ResponseEntity.ok().body(user);
    }

}
