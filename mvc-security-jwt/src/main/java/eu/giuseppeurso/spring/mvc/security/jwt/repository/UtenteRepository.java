package eu.giuseppeurso.spring.mvc.security.jwt.repository;

import eu.giuseppeurso.spring.mvc.security.jwt.model.Utente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UtenteRepository extends JpaRepository<Utente, Long> {

    Utente findByUsername(String username);
}
