package eu.giuseppeurso.spring.mvc.security.jwt.model;

import jakarta.persistence.*;
//import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import static jakarta.persistence.GenerationType.AUTO;

@Entity
@Table(name = "UTENTE")
//@Data
public class Utente implements Serializable {


    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @Column(nullable = false)
    private Integer cancellato;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String tipo;

    @Column
    private String cf;

    @Column
    private String nome;

    @Column
    private String cognome;

    @Column
    private String email;

    @Column
    private String note;

    public Utente() {
    }

    public Utente(Long id, Integer cancellato, String username, String password, String tipo, String cf, String nome, String cognome, String email, String note) {
        this.id = id;
        this.cancellato = cancellato;
        this.username = username;
        this.password = password;
        this.tipo = tipo;
        this.cf = cf;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCancellato() {
        return cancellato;
    }

    public void setCancellato(Integer cancellato) {
        this.cancellato = cancellato;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }






}

