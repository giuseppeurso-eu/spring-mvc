package eu.giuseppeurso.spring.mvc.security.jwt.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import eu.giuseppeurso.spring.mvc.security.jwt.model.Utente;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface UtenteService {

    Utente save(Utente utente);
    Utente findByUsername(String username);
    List<Utente> findAll();
    //Map<String,String> refreshToken(String authorizationHeader, String issuer) throws BadJOSEException, ParseException, JOSEException;
}
