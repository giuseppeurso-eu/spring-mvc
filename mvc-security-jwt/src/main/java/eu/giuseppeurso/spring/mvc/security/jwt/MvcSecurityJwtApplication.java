package eu.giuseppeurso.spring.mvc.security.jwt;

import eu.giuseppeurso.spring.mvc.security.jwt.model.Ruolo;
import eu.giuseppeurso.spring.mvc.security.jwt.model.Utente;
import eu.giuseppeurso.spring.mvc.security.jwt.service.UtenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * https://www.vincenzoracca.com/blog/framework/spring/jwt/
 *
 * https://github.com/buingoctruong/springboot3-springsecurity6-jwt/tree/master/src/main/java/com/truongbn/security
 *
 */
@SpringBootApplication
public class MvcSecurityJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcSecurityJwtApplication.class, args);
	}

	@Autowired
	private Environment env;

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


	@Bean
	CommandLineRunner run(UtenteService utenteService) {
		// Solo al primo avvio
		// Crea due utenti e due ruoli sul DB
		return args -> {
			createUsers(utenteService);

		};
    }


	private void createUsers(UtenteService utenteService) {
//			roleService.save(new Role(null, "ROLE_USER"));
//			roleService.save(new Role(null, "ROLE_ADMIN"));
//
//			userService.save(new User(null, "rossi", "1234", new ArrayList<>()));
//			userService.save(new User(null, "bianchi", "1234", new ArrayList<>()));
//
//			userService.addRoleToUser("rossi", "ROLE_USER");
//			userService.addRoleToUser("bianchi", "ROLE_ADMIN");
//			userService.addRoleToUser("bianchi", "ROLE_USER");
		boolean isUserCreationEnabled= Boolean.parseBoolean(env.getProperty("levidi.initUsers"));
		if(isUserCreationEnabled){
			utenteService.save(new Utente(null,0,"giuseppe","12345", Ruolo.ROLE_ADMIN, "CFDESREE","Giuseppe","Urso",null,null));
		}
	}
}
