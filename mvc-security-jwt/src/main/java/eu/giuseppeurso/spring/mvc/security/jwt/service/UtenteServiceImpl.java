package eu.giuseppeurso.spring.mvc.security.jwt.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import eu.giuseppeurso.spring.mvc.security.jwt.model.Utente;
import eu.giuseppeurso.spring.mvc.security.jwt.repository.UtenteRepository;
import eu.giuseppeurso.spring.mvc.security.jwt.utils.JwtUtil;
//import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
@Transactional
//@Slf4j
public class UtenteServiceImpl implements UtenteService, UserDetailsService {

    private static final String USER_NOT_FOUND_MESSAGE = "User with username %s not found";

    @Autowired
    private final UtenteRepository utenteRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;


    public UtenteServiceImpl(UtenteRepository utenteRepository, PasswordEncoder passwordEncoder) {
        this.utenteRepository = utenteRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utente utente = utenteRepository.findByUsername(username);
        if(utente == null) {
            String message = String.format(USER_NOT_FOUND_MESSAGE, username);
            //log.error(message);
            throw new UsernameNotFoundException(message);
        } else {
            //log.debug("User found in the database: {}", username);
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(utente.getTipo()));
//            user.getRoles().forEach(role -> {
//                authorities.add(new SimpleGrantedAuthority(role.getName()));
//            });
            User authenticatedUser = new User(utente.getUsername(), utente.getPassword(), authorities);
            return authenticatedUser;
        }
    }

    @Override
    public Utente save(Utente user) {
        //log.info("Saving user {} to the database", user.getUsername());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        // eventualmente altri campi da trattare qui...

        return utenteRepository.save(user);
    }



    @Transactional(readOnly = true)
    @Override
    public Utente findByUsername(String username) {
        //log.info("Retrieving user {}", username);
        return utenteRepository.findByUsername(username);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Utente> findAll() {
        //log.info("Retrieving all users");
        return utenteRepository.findAll();
    }



    @Transactional(readOnly = true)
    //@Override
    public Map<String,String> refreshToken(String authorizationHeader, String issuer) throws BadJOSEException,
            ParseException, JOSEException {

        String refreshToken = authorizationHeader.substring("Bearer ".length());
        UsernamePasswordAuthenticationToken authenticationToken = JwtUtil.parseToken(refreshToken);
        String username = authenticationToken.getName();
        Utente user = findByUsername(username);
        //List<String> roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());
        List<String> roles = new ArrayList<>();
        roles.add(user.getTipo());
        String accessToken = JwtUtil.createAccessToken(username, issuer, roles);
        return Map.of("access_token", accessToken, "refresh_token", refreshToken);
    }


}
