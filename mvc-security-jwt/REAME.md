#  Project mvc-security-jwt

### SpringBoot and the JWT Authentication
JWT Token-based authentication with Springboot.
* SpringBoot 3.1.5
* Mysql 8.2
* Nimbus JOSE+JWT 9.37
